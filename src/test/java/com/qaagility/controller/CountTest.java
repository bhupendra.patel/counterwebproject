package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class CountTest {

    @Test
    public void testCounter() throws Exception {

        final int value = new Count().counter(18, 2);
        assertEquals("Test Counter", 9, value);
        
    }
    
    @Test
    public void testCounterMax() throws Exception {

        final int value = new Count().counter(18, 0);
        assertEquals("Test Counter", Integer.MAX_VALUE, value);
        
    }
}
