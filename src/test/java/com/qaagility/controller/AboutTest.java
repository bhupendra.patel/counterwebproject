package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutTest {
    @Test
    public void testDesc() throws Exception {

        final String textInAbout = new About().desc();
        assertTrue("AboutPage", textInAbout.toString().contains("This application"));
        
    }
}
